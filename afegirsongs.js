$("button#botoAfegirCanco").on("click", function(event) {
    event.preventDefault();
    $.ajax({
        method: "POST",
        url: "afegirsongs.php",
        data: {"canco": $("#canco").val()},
        dataType: "json",
        success: function(data) {
            console.log(data);
            let x = $("#x");
            let d = $("#d");
            d.empty();

            if (data.estat === 'OK') {
                x.html("La cançó " + data.nom + " s'ha afegit correctament");
            } else {
                x.empty();
                d.html(data.error);
            }

            setTimeout(reset, 2000);
        },
        error: function(jqXHR, textStatus, error) {
            console.log(jqXHR);
            alert("Error: " + textStatus + " " + error);
        }
    });
});

function reset() {
    location.reload();
}
