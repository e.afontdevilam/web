$("button#botoAfegirReproduccio").on("click", function() {
    event.preventDefault();
    $.ajax({
        method: "POST",
        url: "crear_reproduccio.php.php",
        dataType: "json",

        success: function(data) {
            console.log(data);
            let x = document.getElementById("x");
            let d = document.getElementById("d");
            d.innerHTML = "";
            if (data.idusuari && data.song) {
                x.innerHTML = "L'usuari " + data.idusuari + " ha incrementat les reproduccions d'una cançó: " + data.song;
            } else {
                x.innerHTML = "Hi ha hagut un error en incrementar les reproduccions.";
            }

            setTimeout(reset, 2000);
            window.stop();
        },
        error: function(jqXHR, textStatus, error) {
            console.log(jqXHR);
            alert("Error: " + textStatus + " " + error);
        }
    });
});
