<?php
require_once ("config.php");

$nom = $_POST["canco"];
$contrasenyes = ["estat" => "", "error" => "", "nom" => $nom];

try {
    $pattern = "/^.+$/u";
    $conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);
    $query = $conn->prepare("SELECT * FROM songs WHERE nom = :canco");
    $query->bindParam(":canco", $nom, PDO::PARAM_STR);
    $query->execute();
    $result = $query->fetch(PDO::FETCH_ASSOC);

    if (preg_match($pattern, $nom)) {
        if (!$result) {
            $insertQuery = $conn->prepare("INSERT INTO songs (nom) VALUES (:nom)");
            $insertQuery->bindParam(":nom", $nom, PDO::PARAM_STR);
            $insertQuery->execute();
            $contrasenyes["estat"] = "OK";
            $contrasenyes["error"] = "Cançó afegida amb èxit";
        } else {
            $contrasenyes["estat"] = "KO";
            $contrasenyes["error"] = "La cançó ja existeix";
        }
    } else {
        $contrasenyes["estat"] = "KO";
        $contrasenyes["error"] = "Nom de cançó no vàlid";
    }

    echo json_encode($contrasenyes);
} catch (PDOException $e) {
    echo json_encode("Connection failed: " . $e->getMessage());
}
?>
