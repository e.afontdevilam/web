$("button#boto1").on("click", function() {
    event.preventDefault();
    $.ajax({
        method: "POST",
        url: "login.php",
        data: {"nom": $("#nom").val(), "contrasenya": $("#contrasenya").val(), "usuari": localStorage.getItem('usuari')},
        dataType: "json",

        success: function (data) {
            let a = document.getElementById("a");
            a.innerHTML = '<p>';
            a.append(data.estat);
            a.innerHTML += '<br>';
            a.append(data.error);
            a.innerHTML += '<br>';
            console.log("aaaa");
            console.log(data);
            if (data.estat === 'OK') {
                console.log(localStorage);
                a.innerHTML += '<span style="color: green">' + data.nom;
                localStorage.setItem('usuari', data.nom);

                // Redirige a la página deseada después de un inicio de sesión exitoso
                setTimeout(function() {
                    window.location.href = 'JOJOWEB.html';
                }, 2000);
            } else if (data.estat === 'KO') {
                a.innerHTML += '<span style="color: red">' + data.nom;
            } else {
                a.innerHTML += '<span style="color: red">' + "L'usuari actual es el " + localStorage.getItem('usuari');
            }

            a.innerHTML += '</p>';
        },
        error: function (jqXHR, textStatus, error) {
            console.log(jqXHR);
            alert("Error: " + textStatus + " " + error);
        }
    });
});

$("button#boto3").on("click", function() {
    if (localStorage.getItem('usuari') !== null) {
        console.log(localStorage);
        localStorage.clear();
        document.getElementById("a").innerHTML = '<h3 class="d-flex justify-content-center mt-3 text-success">' + "Has tancat la teva sessió" + '</h3>';
    } else {
        document.getElementById("a").innerHTML = '<h3 class="d-flex justify-content-center mt-3 text-danger">' + "No hi ha sessions activa" + '</h3>';
    }
});
