<?php
require_once ("config.php");

$nom = $_POST["nom"];
$credencial = $_POST["contrasenya"];
$usuari = $_POST["usuari"];
$response = array("estat" => "", "error" => "", "nom" => $usuari);

try {
    $conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);
    $query = $conn->prepare("SELECT * FROM usuaris_app WHERE nom = :nom");
    $query->bindParam(":nom", $nom, PDO::PARAM_STR);
    $result = $query->execute();
    $result = $query->fetch(PDO::FETCH_ASSOC);

    if ($result == "") {
        $response["estat"] = "KO";
        $response["error"] = "Usuari no existeix";
    } elseif ($result["nom"] == $nom && $result["password"] == $credencial) {
        $response["estat"] = "OK";
    } elseif ($result["nom"] == $nom && $result["password"] != $credencial) {
        $response["estat"] = "KO";
        $response["error"] = "Credencial incorrecte";
    } else {
        $response["estat"] = "KO";
        $response["error"] = "Error desconegut";
    }

    print_r(json_encode($response));

} catch (PDOException $e) {
    print_r(json_encode("Connection failed: " . $e->getMessage()));
}
?>
