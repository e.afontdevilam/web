$("button#boto").on("click", function(event) {
    event.preventDefault();
    $.ajax({
        method: "POST",
        url: "SONGS.php",
        data: {"canco": $("#canco").val()},
        dataType: "json",
        success: function(data) {
            console.log(data);
            if (data.estat === "KO") {
                let a = document.getElementById("finale");
                a.innerHTML = '<p>' + "La cançó que has escrit no està a la base de dades. Si us plau, escriu una cançó vàlida" + '</p>';
                setTimeout(reset, 2000);
            } else {
                let taula = "<table><tr><th>Id</th><th>Nom</th><th>Reproduccions</th><th>Recaptació</th></tr>";

                taula += "<tr><td style='padding: 0 15px'>" + data.idSongs + "</td><td style='padding: 0 15px'>" + data.Nom + "</td><td style='padding: 0 15px'>" + data.Reproduccions + "</td><td style='padding: 0 15px'>" + data.Recaptacio + "</td></tr>";

                taula += "</table>";
                document.getElementById("finale").innerHTML = taula;
            }
        },
        error: function(jqXHR, textStatus, error) {
            console.log(jqXHR);
            alert("Error: " + textStatus + " " + error);
        }
    });
});

function reset() {
    location.reload();
}
