function mostrarMensaje() {
    var usuario = document.getElementById("usu").value;
    var contrasena = document.getElementById("contra").value;

    var mensajeHost = `L'usuari ${usuario} ha accedit a l'aplicació amb la contrasenya ${contrasena}. PULSI 'ACCEPTAR', SI US PLAU.`;

    alert(mensajeHost);

    return true;
}

function cancelarFormulario() {
    var mensajeCancelacion = "L'usuari ha cancel·lat l'operació. PULSI 'ACCEPTAR', SI US PLAU.";
    alert(mensajeCancelacion);

    window.location.href = "JOJOWEB.html#fondosur";
}
