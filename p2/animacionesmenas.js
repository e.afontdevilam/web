$(document).ready(function() {
    var animatedElement = $("#keytoo");

    animatedElement.animate({
        width: "1338px",
        height: "754px",
        dummyProp: 1
    }, {
        duration: 1000,  // Duración total de la animación
        step: function(now, fx) {
            if (fx.prop === "dummyProp") {
                animatedElement.css("transform", "rotate(" + now * 360 + "deg)");
            }
        },
        complete: function() {
            // Esperar 7 segundos y luego mostrar la alerta
            setTimeout(function() {
                alert("Ups, parece ser que no hay una verdad absoluta en este campo y que depende de cada uno elegir cómo quiere representar musical, estética y personalmente a su personaje, ya que eso es lo que lo hace realmente suyo y lo que lo diferenciará del resto (a no ser que cometamos delitos de copyright o contra la propiedad intelectual de otras entidades, ahí ya cada uno que se responsabilice de sus actos). Es un poco como decir que el One Piece son los amigos que hicimos por el camino, pero qué le vamos a hacer. Si hubiera un secreto de este estilo, todo el mundo estaría ahora mismo creando personajes en masa y de manera sistemática, arrebatándoles el encanto intrínseco que los autores se esfuerzan por conseguir crear para hacer personajes memorables. Anda, haz click sobre DIO...");
            }, 3000);
        }
    });
});
