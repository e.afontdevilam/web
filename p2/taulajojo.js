document.addEventListener("DOMContentLoaded", function () {
    var nombre = "JOJO";
    var tema = "TEMA";
    var stand = "STAND";

    var tabla = document.createElement("table");
    tabla.style.border = "5px solid white"; // Añadir el borde blanco a la tabla

    var fila1 = tabla.insertRow();
    var celda1 = fila1.insertCell(0);
    var celda2 = fila1.insertCell(1);
    var celda3 = fila1.insertCell(2);

    celda1.style.color = "white";
    celda2.style.color = "white";
    celda3.style.color = "white";
    celda1.innerHTML = nombre;
    celda2.innerHTML = tema;
    celda3.innerHTML = stand;

    var fila1 = tabla.insertRow();
    var celda1 = fila1.insertCell(0);
    var celda2 = fila1.insertCell(1);
    var celda3 = fila1.insertCell(2);

    celda1.style.color = "white";
    celda2.style.color = "white";
    celda3.style.color = "white";
    celda1.innerHTML = "Jonathan Joestar";
    celda2.innerHTML = "Fukutsu Mushinno Sakebi";
    celda3.innerHTML = "Hammon (no es un stand)";


    var fila1 = tabla.insertRow();
    var celda1 = fila1.insertCell(0);
    var celda2 = fila1.insertCell(1);
    var celda3 = fila1.insertCell(2);

    celda1.style.color = "white";
    celda2.style.color = "white";
    celda3.style.color = "white";
    celda1.innerHTML = "Joseph Joestar";
    celda2.innerHTML = "Overdrive";
    celda3.innerHTML = "Hermit Purple";


    var fila1 = tabla.insertRow();
    var celda1 = fila1.insertCell(0);
    var celda2 = fila1.insertCell(1);
    var celda3 = fila1.insertCell(2);

    celda1.style.color = "white";
    celda2.style.color = "white";
    celda3.style.color = "white";
    celda1.innerHTML = "Jotaro Kujo";
    celda2.innerHTML = "Stardust Crussaders";
    celda3.innerHTML = "Star Platinum";


    var fila1 = tabla.insertRow();
    var celda1 = fila1.insertCell(0);
    var celda2 = fila1.insertCell(1);
    var celda3 = fila1.insertCell(2);

    celda1.style.color = "white";
    celda2.style.color = "white";
    celda3.style.color = "white";
    celda1.innerHTML = "Josuke Higashikata";
    celda2.innerHTML = "Diamond is Unbrekeable";
    celda3.innerHTML = "Crazy Diamond";


    var fila1 = tabla.insertRow();
    var celda1 = fila1.insertCell(0);
    var celda2 = fila1.insertCell(1);
    var celda3 = fila1.insertCell(2);

    celda1.style.color = "white";
    celda2.style.color = "white";
    celda3.style.color = "white";
    celda1.innerHTML = "Giorno Giovanna";
    celda2.innerHTML = "Il vento d'oro";
    celda3.innerHTML = "Gold Experience";


    var fila1 = tabla.insertRow();
    var celda1 = fila1.insertCell(0);
    var celda2 = fila1.insertCell(1);
    var celda3 = fila1.insertCell(2);

    celda1.style.color = "white";
    celda2.style.color = "white";
    celda3.style.color = "white";
    celda1.innerHTML = "Jolyne Cujoh";
    celda2.innerHTML = "Stone Ocean";
    celda3.innerHTML = "Stone Free";

    var ubicacionTablilla = document.getElementById("ubicacion-tablilla");

    ubicacionTablilla.appendChild(tabla);
});
