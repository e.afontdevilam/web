const newspaperTiming = {
    duration: 5000,
    iterations: Infinity
};

const newspaperSpinning = [
    { marginLeft: "50px" },
    { marginLeft: "9px" },
    { marginLeft: "50px" }
];

const newspaperTimingh = {
    duration: 20000,
    iterations: Infinity
};

const newspaperSpinningh = [
    { marginLeft: "890px" },
    { marginLeft: "9px" },
    { marginLeft: "890px" }
];

let moviments = [
    document.getElementById("videogiorno"),
    document.getElementById("videojotaro"),
    document.getElementById("videojosuke"),
    document.getElementById("videojonathan"),
    document.getElementById("videojoseph"),
    document.getElementById("videojolyne"),
    document.getElementById("killerqueeen")
];

moviments.forEach((moviment) => {
    moviment.animate(newspaperSpinning, newspaperTiming);
});

// Verificar tamaño de la pantalla
window.addEventListener('resize', mirartamaño);

mirartamaño();

function mirartamaño() {
    const winWidth = window.innerWidth;
    const minWidth = 576;

    if (winWidth >= minWidth) {
        ejecutar();
    } else {
        stopani();
    }
}

function ejecutar() {
    moviments.forEach((moviment) => {
        moviment.animate(newspaperSpinning, newspaperTiming);
    });
}

function stopani() {
    moviments.forEach((moviment) => {
        moviment.getAnimations().forEach(animation => animation.cancel());
    });
}
