function hora() {
    const ahora = new Date();

    const year = ahora.getFullYear().toString();
    const month = (ahora.getMonth() + 1).toString().padStart(2, '0');
    const day = ahora.getDate().toString().padStart(2, '0');

    const hora = ahora.getHours().toString().padStart(2, '0');
    let minutos = ahora.getMinutes().toString().padStart(2, '0');
    let segundos = ahora.getSeconds().toString().padStart(2, '0');

    const fecha = year + "-" + month + "-" + day;
    const horaCompleta = hora + ":" + minutos + ":" + segundos;

    const html = fecha + " " + horaCompleta;

    const relojElement = document.getElementById("reloj");

    relojElement.innerHTML = html;
    relojElement.style.fontSize = "25px";
    relojElement.style.color = 'white';
}
hora();
setInterval(hora, 1000);
