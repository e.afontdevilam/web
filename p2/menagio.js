let moviment = document.getElementById("menacing");

const newspaperTiming = {
    duration: 700,
    iterations: Infinity
};

const newspaperSpinning = [
    { marginTop: "25px" },
    { marginTop: "9px" },
    { marginTop: "25px" }
];

moviment.animate(newspaperSpinning, newspaperTiming);