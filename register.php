<?php
require_once ("config.php");

$nom = $_POST["nom"];
$credencial = $_POST["contrasenya"];
$contrasenyes = Array(Array("estat"=>""), Array("error"=>""), Array("nom"=>$nom));

try {
    $pattern = "/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/";
    $conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);
    $query = $conn->prepare("SELECT * FROM usuaris_app WHERE nom = :correu");
    $query->bindParam(":correu", $nom, PDO::PARAM_STR);
    $result = $query->execute();
    $result = $query->fetch(PDO::FETCH_ASSOC);

    if (preg_match($pattern, $nom) && $result == "") {
        // Inicia la sesión PHP
        session_start();

        // Guarda el nombre de usuario en $_SESSION
        $_SESSION['nom'] = $nom;

        $query = $conn->prepare("INSERT INTO usuaris_app (nom, password) VALUES (:nom, :contrasenya)");
        $query->bindParam(":nom", $nom, PDO::PARAM_STR);
        $query->bindParam(":contrasenya", $credencial, PDO::PARAM_STR);
        $query->execute();

        $contrasenyes[0]["estat"] = "OK";
        $contrasenyes[1]["error"] = "T'has enregistrat amb èxit";
        print_r(json_encode($contrasenyes));
    } else if (preg_match($pattern, $nom) && $result != "") {
        $contrasenyes[0]["estat"] = "KO";
        $contrasenyes[1]["error"] = "L'usuari existeix";
        print_r(json_encode($contrasenyes));
    } else {
        $contrasenyes[0]["estat"] = "KO";
        $contrasenyes[1]["error"] = "Correu incorrecte";
        print_r(json_encode($contrasenyes));
    }

} catch(PDOException $e) {
    print_r(json_encode("Connection failed: " . $e->getMessage()));
}
?>
