<?php
require_once ("config.php");

try {
    $conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);


    $query_users = $conn->query("SELECT idusuari FROM usuaris_songs ORDER BY RAND() LIMIT 1");
    $user = $query_users->fetch(PDO::FETCH_ASSOC);


    $query_songs = $conn->query("SELECT id, nom FROM songs ORDER BY RAND() LIMIT 1");
    $song = $query_songs->fetch(PDO::FETCH_ASSOC);


    $reproduccions = json_decode($user['reproduccions'], true);
    $reproduccions[] = array("song" => $song['id'], "reproduccions" => 1);
    $reproduccions_json = json_encode($reproduccions);

    $query_update = $conn->prepare("UPDATE usuaris_songs SET reproduccions = :reproduccions WHERE idusuari = :idusuari");
    $query_update->bindParam("reproduccions", $reproduccions_json, PDO::PARAM_STR);
    $query_update->bindParam("idusuari", $user['idusuari'], PDO::PARAM_STR);
    $query_update->execute();


    $response = array("idusuari" => $user['idusuari'], "song" => $song['nom']);
    print_r(json_encode($response));

} catch(PDOException $e) {
    print_r(json_encode("Connection failed: " . $e->getMessage()));
}
?>

