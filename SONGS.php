<?php
$servername = "localhost";
$username = "root";
$password = "super3";

$nom = $_POST["canco"];

$contrasenyes = array("estat" => "", "error" => "", "nom" => $nom);

try {
    $conn = new PDO("mysql:host=$servername;dbname=albums", $username, $password);
    $query = $conn->prepare("SELECT * FROM songs WHERE nom = :canco");
    $query->bindParam(":canco", $nom, PDO::PARAM_STR);
    $result = $query->execute();
    $result = $query->fetch(PDO::FETCH_ASSOC);

    if ($result == false) {
        $contrasenyes["estat"] = "KO";
        $contrasenyes["error"] = "La cançó no existeix";
    } else {
        $contrasenyes["estat"] = "OK";
        $contrasenyes["idSongs"] = $result["idSongs"];
        $contrasenyes["Nom"] = $result["Nom"];
        $contrasenyes["Reproduccions"] = $result["Reproduccions"];
        $contrasenyes["Recaptacio"] = $result["Recaptacio"];
    }

    echo json_encode($contrasenyes);
} catch (PDOException $e) {
    echo json_encode("Connection failed: " . $e->getMessage());
}
?>
