$("button#boton1").on("click", function() {
    event.preventDefault();

    if (localStorage.getItem('nom') === null) {
        $.ajax({
            method: "POST",
            url: "register.php",
            data: {"nom": $("#nom").val(), "contrasenya": $("#contrasenya").val()},
            dataType: "json",

            success: function (data) {
                console.log(data);
                let x = document.getElementById("formulariLogin");
                x.innerHTML = '<p>';
                x.append(data[0].estat);
                x.innerHTML += '<br>';
                x.append(data[0].error);
                x.innerHTML += '<br>';

                if (data[0].estat === 'OK') {
                    x.innerHTML += '<span style="color: green">' + data[0].nom;
                    localStorage.setItem('nom', data[0].nom);

                    // Redirige a la página deseada después del registro exitoso
                    setTimeout(function() {
                        window.location.href = 'JOJOWEB.html';
                    }, 2000);
                } else {
                    x.innerHTML += '<span style="color: red">' + data[0].nom;
                    setTimeout(espera, 3000);
                }
            },
            error: function (jqXHR, textStatus, error){
                console.log(jqXHR);
                alert("Error: " + textStatus + " " + error);
            }
        });
    } else {
        let x = document.getElementById("formulariLogin");
        x.innerHTML = '<p class="d-flex justify-content-center text-danger mt-3">' + "Ja n'hi ha una sessió oberta. Tanca la sessió actual si vols usar una de diferent." + '</p>';
        setTimeout(espera, 3000);
    }
});

// Resto del código sigue igual...
